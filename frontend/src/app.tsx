import s from './app.module.scss'
import {Form} from "./components/shared/form/form.tsx";

export const App = () => {
    return (
        <div className={s.app}>
            <Form/>
        </div>
    )
}