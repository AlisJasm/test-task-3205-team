import s from './button.module.scss'

export const Button = () => {
    return (
        <button className={s.button}>
            Submit
        </button>
    )
}