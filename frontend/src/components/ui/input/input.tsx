import s from './input.module.scss'
import InputMask from 'react-input-mask'
import {ComponentProps, forwardRef} from "react";

type Props ={
    placeholder: string
    mask?: string
} & Omit<ComponentProps<"input">, 'ref'>

export const Input = forwardRef<HTMLInputElement, Props>(
    ({placeholder, mask, ...inputProps},
     ref
    ) => {

    return (
        mask ?
            <InputMask mask={mask} type="text" className={s.input} placeholder={placeholder} inputRef={ref} {...inputProps}/>
            :
            <input type="text" className={s.input} placeholder={placeholder} ref={ref} {...inputProps}/>
    )
})