import s from './form.module.scss'
import { Input } from '../../ui/input/input.tsx'
import { Button } from '../../ui/button/button.tsx'
import { SubmitHandler, useForm } from 'react-hook-form'
import { getUser } from '../../../api/get-users.ts'
import { useRef, useState } from 'react'
import { User } from '../../../types/users.ts'

type FormType = {
  email: string
  number: string
};

export const Form = () => {
  const [users, setUsers] = useState<User[]>([])
  const [error, setError] = useState('')
  const [isLoaded, setIsLoaded] = useState(false)
  const abortControllerRef = useRef<AbortController | null>(null)

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<FormType>()
  const onSubmit: SubmitHandler<FormType> = async data => {
    abortControllerRef.current?.abort()
    setUsers([])
    setIsLoaded(false)
    const number = data.number.replaceAll('-', '').replaceAll('_', '')
    if (number.length < 6 && number.length !== 0) {
      setError('The number must be 6 characters long')
    } else {
      setError('')
      abortControllerRef.current = new AbortController()
      setUsers(await getUser(data.email, number, abortControllerRef.current!))
      setIsLoaded(true)
      console.log(111111111)
    }
  }

  return (
    <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
      <Input placeholder="Email"
             {...register(
               'email',
               {
                 required: 'Email Address is required',
                 pattern: {
                   value: /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/,
                   message: 'Invalid email address'
                 }
               }
             )
             }/>
      {errors.email?.message}
      <Input placeholder="Number" mask="99-99-99" {...register('number')}/>
      {error}
      <Button/>
      <div className={s.result}>
        {users.length !== 0
          ?
          users.map((user, index) => <p className={s.data} key={index}>{JSON.stringify(user)}</p>)
          :
          isLoaded && <p className={s.data}>No matches found</p>
        }
      </div>
    </form>
  )
}