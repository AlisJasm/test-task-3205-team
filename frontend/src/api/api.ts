import axios from 'axios'

const API_PATH = 'http://localhost:8000'

export const api = axios.create({
  baseURL: API_PATH
})

api.interceptors.request.use((config) => {
  return config
})