import { api } from './api'
import { User } from '../types/users.ts'

export const getUser = (email: string, number: string, abortController: AbortController) => {
  return api.get<User[]>(`users`, {
    params:
      {
        email: email,
        number: number
      },
    signal: abortController.signal
  })
    .then(res => res.data)
}