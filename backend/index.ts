import express, { Express, Request, Response } from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import data from './data.json'

const app: Express = express()
const port = process.env.PORT || 8000

app.use(cors())
app.use(bodyParser.json())

app.get('/users', (req: Request, res: Response) => {
  setTimeout(() => {
    const result = data.filter(obj => {
      if (req.query.number === '')
        return obj.email === req.query.email
      return obj.email === req.query.email && obj.number === req.query.number
    })

    res.send(result)
  }, 5000)
})

app.listen(port, () => {
  console.log(`⚡[server]: Server is running at http://localhost:${port}`)
})